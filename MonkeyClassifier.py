from os import listdir
import sys
from numpy import asarray
from numpy import save
import numpy as np
import threading

from tensorflow import keras
import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten, Dropout, GlobalMaxPool2D, MaxPooling2D, Activation, Conv2D, \
    BatchNormalization, GaussianNoise
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import load_img, img_to_array, ImageDataGenerator
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import regularizers

import pandas as pd
import matplotlib.pyplot as plt

import random

IMG_WIDTH = 200
IMG_HEIGHT = 200
BATCH_SIZE = 64
EPOCHS = 200
MONKEY_CLASS_NAMES=["mantled_howler",
                            "patas_monkey",
                            "bald_uakari",
                            "japanese_macaque",
                            "pygmy_marmoset",
                            "white_headed_capuchin",
                            "silvery_marmoset",
                            "common_squirrel_monkey",
                            "black_headed_night_monkey",
                            "nilgiri_langur"]

def init_imagegen():
    datagen = ImageDataGenerator(rescale=1.0 / 255.0,
                                 width_shift_range=0.2,
                                 height_shift_range=0.2,
                                 rotation_range=40,
                                 shear_range=0.2,
                                 zoom_range=0.2,
                                 validation_split=0.2,
                                 horizontal_flip=True,
                                 channel_shift_range=155,
                                 fill_mode='nearest')
    return datagen


def init_train_flow(datagen,batch_size=BATCH_SIZE):
    return datagen.flow_from_directory("data\\training\\training", class_mode='categorical',
                                       batch_size=batch_size, shuffle=True, seed=int(random.random()*9999%1000),
                                       target_size=(IMG_WIDTH, IMG_HEIGHT))
def init_evaluate_flow(batch_size=BATCH_SIZE):
    return ImageDataGenerator(rescale=1.0 / 255.0).flow_from_directory("data\\evaluation",class_mode='categorical',
                                       shuffle=True, seed=int(random.random()*9999%1000), batch_size=batch_size, target_size=(IMG_WIDTH, IMG_HEIGHT))


def init_plot_flow(dir):
    return init_imagegen().flow_from_directory(dir,
                                                    class_mode='categorical',
                                                    #shuffle=True, seed=int(random.random()*9999%1000),
                                                    batch_size=1, target_size=(IMG_WIDTH, IMG_HEIGHT))

def init_validate_flow(datagen,batch_size=BATCH_SIZE):
    return datagen.flow_from_directory("data\\validation\\validation\\", class_mode='categorical',
                                       shuffle=True, seed=int(random.random()*9999%1000), batch_size=batch_size, target_size=(IMG_WIDTH, IMG_HEIGHT))


class PlottingCallback(keras.callbacks.Callback):
    def __init__(self):
        super(keras.callbacks.Callback, self).__init__()
    def on_epoc_begin(self, logs=None):
        print("epoch ended!! do the weights thing noww!!")
        for index,layer in enumerate(self.model.layers[:1]):
                print("layer[%d] : %s" % (index,layer.get_weights()))


def build_callbacks():
    return [keras.callbacks.ModelCheckpoint(filepath="checkpoints\\weights-improvement-{epoch:02d}-{val_accuracy:.2f}.hdf5",monitor='accuracy',save_best_only=True,mode='auto',save_freq='epoch'),PlottingCallback()]


def build_model():
    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(IMG_WIDTH, IMG_HEIGHT, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(rate=0.2))
    model.add(BatchNormalization())

    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(1024, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(rate=0.2))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    model.summary()

    model.compile(optimizer=Adam(learning_rate=0.001),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    return model


def fit(model=None, train_flow=None, test_flow=None):
    out = model.fit(train_flow, callbacks=build_callbacks(),
                    steps_per_epoch=len(train_flow),
                    validation_data=test_flow,
                    validation_steps=len(test_flow),
                    epochs=EPOCHS,
                    shuffle=True,
                    verbose=1)
    model.save_weights("monkey_classifier_weights%s.h5" % (int(random.random() * 999 % 300)))
    return out
def vec2abel(vec):
        return MONKEY_CLASS_NAMES[np.where(vec[0] >= 1.0)[0][0]]

def make_predictions(model=None,flow=None):
    images = []
    labels = []
    image, label = flow.next()
    index  = 0
    while len(image) != 0:
            images.append(image[0])
            label = model.predict_classes(image)
            #print("image %s => %s" % (index, label))
            labels.append(label)
            index += 1
            if index > 24:
                break
            image, label = flow.next()
    plot_predictions(images=images,labels=labels)

def plot_predictions(images=None,labels=None,n_cols=4,n_rows=4):
    index = 0
    image, label = images[index], labels[index]
    index += 1
    #figure, axis = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 15))
    while len(image) != 0:
        figure, axis = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 15))
        for a in axis:
            for p in a:
                try:
                    #if len(image) == 0 or len(label) == None or index > 23:
                    #    return

                    p.imshow(image)
                    p.set_title("%s" % (MONKEY_CLASS_NAMES[label[0]]))
                    image, label = images[index],labels[index]
                    index+=1
                    #   image = image.astype('uint8')
                except:
                    pass
        plt.show()
    print("done plotting")
    return

def plot_flow(flow=None,n_cols=5,n_rows=8,title="Plot"):
    image, label = flow.next()
    figure, axis = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(15, 15))
    for a in axis:
        for p in a:
            if len(image[0]) == 0 or len(label) == None:
                return
            p.imshow(image[0])
            p.set_title("class:%s" % (vec2abel(label)))
            image, label = flow.next()
            # image = image.astype('uint8')

    plt.show()
    print("done plotting")
    return

def init_plot_flows(n_cols=4,n_rows=4):
    train_flow = init_plot_flow("data\\training\\training\\")
    valid_flow = init_plot_flow("data\\validation\\validation\\")
    plot_flow(flow=train_flow,n_cols=n_cols,n_rows=n_rows,title="Training Flow")
    plot_flow(flow=valid_flow,n_cols=n_cols,n_rows=n_rows, title="Validation Flow")
    return

def plot_training(history=None):
    plot_tag = "%s" % ((int(random.random() * 999 % 300)))
    data = pd.DataFrame(history)
    plt.figure("accuracy_%s" % (plot_tag), figsize=(15, 8))
    plt.plot(data['accuracy'], 'r--')
    plt.title('Accuracy Plot')
    plt.grid()
    plt.plot(data['val_accuracy'], 'b--')
    plt.legend(['training', 'validation'])
    plt.figure("loss_%s" % (plot_tag), figsize=(15, 8))
    plt.plot(data['loss'], 'r--')
    plt.plot(data['val_loss'], 'b--')
    plt.title('Loss Plot')
    plt.grid()
    plt.legend(['training', 'validation']);
    plt.show()


if __name__ == "__main__":


    model = build_model()
    imagegen = init_imagegen()
    train_fl = init_train_flow(imagegen)
    valid_fl = init_validate_flow(imagegen,batch_size=1)
    #evaluate_fl = init_evaluate_flow(batch_size=1)
    #init_plot_flows()
    history = fit(model,
                 train_flow=train_fl,
                  test_flow=valid_fl)

    #model.load_weights(filepath="checkpoints\\weights-improvement-120-0.69.hdf5")
    #make_predictions(model,flow=evaluate_fl)
    plot_training(history.history)


