from os import listdir
import sys
from numpy import asarray
from numpy import save

from tensorflow import keras
import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten, Dropout, GlobalMaxPool2D, MaxPooling2D, Activation, Conv2D, BatchNormalization,GaussianNoise
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import load_img, img_to_array, ImageDataGenerator
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import regularizers

import pandas as pd
import matplotlib.pyplot as plt

import random
IMG_WIDTH=150
IMG_HEIGHT=150


def init_imagegen():
    datagen = ImageDataGenerator(rescale=1.0/255.0,
                                    width_shift_range=0.2,
                                    height_shift_range=0.2,
                                    rotation_range=40,
                                    shear_range=0.2,
                                    zoom_range=0.2,
                                    validation_split=0.2,
                                    horizontal_flip=True,
                                    fill_mode='nearest')
    return datagen

def init_train_flow(datagen):
    return datagen.flow_from_directory("data\\train\\", class_mode='binary', batch_size=64, shuffle=True, seed=137616, target_size=(IMG_WIDTH, IMG_HEIGHT))
def init_test_flow(datagen):
    return ImageDataGenerator(rescale=1.0/255.0, fill_mode='nearest').flow_from_directory("data\\test\\", class_mode='binary', shuffle=True, seed=137616, batch_size=64, target_size=(IMG_WIDTH, IMG_HEIGHT))
def init_validate_flow(datagen):
    return datagen.flow_from_directory("data\\validate\\", class_mode='binary', shuffle=True, seed=342252, batch_size=64, target_size=(IMG_WIDTH, IMG_HEIGHT))

"""
    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(IMG_WIDTH, IMG_HEIGHT, 3)))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))


    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    
    

    model.add(Flatten())
    model.add(Dense(1, activation='sigmoid'))
    model.summary()
    
    #69/69 [==============================] - 39s 565ms/step - loss: 0.2191 - accuracy: 0.9136 - val_loss: 0.2735 - val_accuracy: 0.8775

"""
def build_callbacks():
        return keras.callbacks.TensorBoard(log_dir="logs\\",histogram_freq=0,write_graph=True,write_images=True)
def build_model():
    model = Sequential()
    model.add(Conv2D(16, (3, 3), activation='relu', padding='same', input_shape=(IMG_WIDTH, IMG_HEIGHT, 3)))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))


    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Flatten())
    model.add(Dense(1, activation='sigmoid'))
    model.summary()

    model.compile(optimizer=Adam(learning_rate=0.001),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


def fit(model=None, train_flow=None, test_flow=None):
    out = model.fit(train_flow, callbacks=build_callbacks(),
                               steps_per_epoch=len(train_flow),
                               validation_data=test_flow,
                               validation_steps=len(test_flow),
                               epochs=100,
                               shuffle=True,
                               verbose=1)
    model.save_weights("dogs_vs_cats_weights%s.h5" % (int(random.random()*999%300)))
    return out

def plot_training(history=None):
    plot_tag = "%s" % ((int(random.random()*999%300)))
    data = pd.DataFrame(history)
    plt.figure("accuracy_%s" % (plot_tag), figsize=(15, 8))
    plt.plot(data['accuracy'], 'r--')
    plt.title('Accuracy Plot')
    plt.grid()
    plt.plot(data['val_accuracy'], 'b--')
    plt.legend(['training', 'validation'])
    plt.figure("loss_%s" % (plot_tag), figsize=(15, 8))
    plt.plot(data['loss'], 'r--')
    plt.plot(data['val_loss'], 'b--')
    plt.title('Loss Plot')
    plt.grid()
    plt.legend(['training', 'validation']);
    plt.show()

if __name__ == "__main__":
    model = build_model()
    imagegen = init_imagegen()
    train_fl = init_train_flow(imagegen)
    test_fl = init_test_flow(imagegen)
    valid_fl = init_validate_flow(imagegen)
    history = fit(model,
        train_flow=train_fl,
        test_flow=valid_fl)
    print(history.history)
    plot_training(history.history)


